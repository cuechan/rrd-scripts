#!/usr/bin/python3

# Count devices in a given network

import os
import sys
import netaddr
import subprocess
from pprint import pprint


DATA_DIR = "."
DATA_OUT = "/tmp/"



MINUTE = 60
HOUR   = MINUTE * 60
DAY    = HOUR * 24
MONTH  = DAY * 30
YEAR   = DAY * 365



def is_up(ip_addr):
	devices = subprocess.run(
		[
			"fping",
			"-c", "5",
			"-t", "250",
			str(ip_addr)
		],
		stdout=subprocess.DEVNULL,
		stderr=subprocess.DEVNULL,
	)

	if devices.returncode != 0:
		return False
	else:
		return True



def count_devices(net):
	count = 0

	for addr in network:
		if str(addr).endswith(".0") or addr == network.broadcast:
			continue
		else:
			if is_up(addr):
				count += 1

	return count




def add_data(net, online_devices):
	filename = DATA_DIR+"/"+str(net).replace("/", "_")+".rrd"

	if not os.path.isfile(filename):
		print("no db")
		create_database(filename)

	print("adding data")

	subprocess.run([
		"rrdtool", "update", filename, "N:"+str(online_devices)
	])





def create_database(filename):
	print("creating:", filename)

	subprocess.run([
		"rrdtool", "create", filename,
		"--step", "60",
		"DS:online:GAUGE:120:0:U",
		"RRA:MAX:0.5:1:"+str(DAY),
		"RRA:MAX:0.5:60:"+str(MONTH),
		"RRA:MAX:0.5:240:"+str(YEAR),
	])



def create_graph(net):
	graph_filename = DATA_OUT+"/"+str(net).replace("/", "_")+".png"
	rrd_filename = DATA_DIR+"/"+str(net).replace("/", "_")+".rrd"

	subprocess.run([
		"rrdtool", "graph", graph_filename,
		"-a", "PNG",
		"-w", "1280",
		"-h", "360",
		"--title", str(net),
		"--start", "-12h",
		"DEF:online="+rrd_filename+":online:AVERAGE",
		"AREA:online#00ff00:online_devices"
	])










network = sys.argv[1]

network = netaddr.IPNetwork(network)
print("scanning "+str(network))

counted = count_devices(network)
print(counted, "devices online")
add_data(network, counted)
create_graph(network)
